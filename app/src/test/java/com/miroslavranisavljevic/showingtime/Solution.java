package com.miroslavranisavljevic.showingtime;

import com.miroslavranisavljevic.showingtime.Util.Capability;
import com.miroslavranisavljevic.showingtime.Util.Constants;
import com.miroslavranisavljevic.showingtime.Util.Devices;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URL;
import java.util.List;

import io.appium.java_client.android.AndroidDriver;

public class Solution {

    private static AndroidDriver<WebElement> driver;
    private Devices current_device;

    @Before
    public void setUp() {

        current_device = new Devices(Constants.TESLA_SMARTPHONE_6_1);

        String browser = "Chrome";

        try {
            driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"),
                    Capability.capabilitiesWebLaunch(browser,
                            current_device.getName(),
                            current_device.getVersion()));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void openGoogleLink() throws InterruptedException {
        try {
            driver.get("https://www.google.com");
            Thread.sleep(1000);

        } catch (org.openqa.selenium.WebDriverException exception) {
            tearDown();
        }
    }

    @Test
    public void ButtonGoogleApps() throws InterruptedException {
        String id_button_apps = "gbwa";

        try {
            driver.get("https://www.google.com");
            Thread.sleep(1000);

            WebElement google_apps = driver.findElement(By.id(id_button_apps));
            Assert.assertTrue(google_apps.isDisplayed());

        } catch (org.openqa.selenium.WebDriverException exception) {
            tearDown();
        }
    }

    @Test
    public void ButtonSignIn() throws InterruptedException {
        String button_sign_in = "Sign in";

        try {
            driver.get("https://www.google.com");
            Thread.sleep(1000);

            WebElement google_sign_in = driver.findElement(By.linkText(button_sign_in));
            Assert.assertTrue(button_sign_in, google_sign_in.getText().contains(button_sign_in));
            Assert.assertTrue(google_sign_in.isDisplayed());

        } catch (org.openqa.selenium.WebDriverException exception) {
            tearDown();
        }
    }

    @Test
    public void TabAll() throws InterruptedException {
        String text_all = "ALL";

        try {
            driver.get("https://www.google.com");
            Thread.sleep(1000);

            WebElement tab_all = driver.findElement(By.linkText(text_all));
            Assert.assertTrue(text_all, tab_all.getText().contains(text_all));
            Assert.assertTrue(tab_all.isDisplayed());

        } catch (org.openqa.selenium.WebDriverException exception) {
            tearDown();
        }
    }

    @Test
    public void TabImages() throws InterruptedException {
        String text_images = "IMAGES";

        try {
            driver.get("https://www.google.com");
            Thread.sleep(1000);

            WebElement tab_images = driver.findElement(By.linkText(text_images));
            Assert.assertTrue(text_images, tab_images.getText().contains(text_images));
            Assert.assertTrue(tab_images.isDisplayed());

        } catch (org.openqa.selenium.WebDriverException exception) {
            tearDown();
        }
    }

    @Test
    public void textOffered() throws InterruptedException {
        String offered_id = "SIvCob";
        String offered_in = "Google offered in:";

        try {
            driver.get("https://www.google.com");
            Thread.sleep(1000);

            List<WebElement> offered = driver.findElements(By.id(offered_id));
            Assert.assertTrue(offered_in, offered.get(0).getText().contains(offered_in));
            Assert.assertTrue(offered.get(0).isDisplayed());

        } catch (org.openqa.selenium.WebDriverException exception) {
            tearDown();
        }
    }

    @Test
    public void LinkCyrillic() throws InterruptedException {
        String link_cyrillic = "српски";

        try {
            driver.get("https://www.google.com");
            Thread.sleep(1000);

            WebElement link_1 = driver.findElement(By.linkText(link_cyrillic));
            Assert.assertTrue(link_cyrillic, link_1.getText().contains(link_cyrillic));
            Assert.assertTrue(link_1.isDisplayed());

        } catch (org.openqa.selenium.WebDriverException exception) {
            tearDown();
        }
    }

    @Test
    public void LinkLatinic() throws InterruptedException {
        String link_latinic = "srpski";

        try {
            driver.get("https://www.google.com");
            Thread.sleep(1000);

            WebElement link_2 = driver.findElement(By.linkText(link_latinic));
            Assert.assertTrue(link_latinic, link_2.getText().contains(link_latinic));
            Assert.assertTrue(link_2.isDisplayed());

        } catch (org.openqa.selenium.WebDriverException exception) {
            tearDown();
        }
    }

    @Test
    public void googleSearchTerm() throws InterruptedException {
        String showingTime = "ShowingTime";
        String showingTimeURL = "www.showingtime.com";

        try {
            driver.get("https://www.google.com");
            Thread.sleep(1000);

            driver.findElement(By.name("q")).sendKeys(showingTime);
            driver.findElement(By.className("Tg7LZd")).click();
            Thread.sleep(1000);
            String result = driver.findElement(By.xpath("//div[@id = 'rso']/div")).getText();
            Assert.assertTrue(showingTimeURL, result.contains(showingTimeURL));

        } catch (org.openqa.selenium.WebDriverException exception) {
            tearDown();
        }
    }

    @Test
    public void footerSettings() throws InterruptedException {
        String footer_element = "Settings";

        try {
            driver.get("https://www.google.com");
            Thread.sleep(1000);

                WebElement link_footer = driver.findElement(By.linkText(footer_element));
                Assert.assertTrue(footer_element, link_footer.getText().contains(footer_element));
                Assert.assertTrue(link_footer.isDisplayed());

        } catch (org.openqa.selenium.WebDriverException exception) {
            tearDown();
        }
    }

    @Test
    public void footerPrivacy() throws InterruptedException {
        String footer_element = "Privacy";

        try {
            driver.get("https://www.google.com");
            Thread.sleep(1000);

            WebElement link_footer = driver.findElement(By.linkText(footer_element));
            Assert.assertTrue(footer_element, link_footer.getText().contains(footer_element));
            Assert.assertTrue(link_footer.isDisplayed());

        } catch (org.openqa.selenium.WebDriverException exception) {
            tearDown();
        }
    }

    @Test
    public void footerTerms() throws InterruptedException {
        String footer_element = "Terms";

        try {
            driver.get("https://www.google.com");
            Thread.sleep(1000);

            WebElement link_footer = driver.findElement(By.linkText(footer_element));
            Assert.assertTrue(footer_element, link_footer.getText().contains(footer_element));
            Assert.assertTrue(link_footer.isDisplayed());

        } catch (org.openqa.selenium.WebDriverException exception) {
            tearDown();
        }
    }

    @Test
    public void footerAdvertising() throws InterruptedException {
        String footer_element = "Advertising";

        try {
            driver.get("https://www.google.com");
            Thread.sleep(1000);

            WebElement link_footer = driver.findElement(By.linkText(footer_element));
            Assert.assertTrue(footer_element, link_footer.getText().contains(footer_element));
            Assert.assertTrue(link_footer.isDisplayed());

        } catch (org.openqa.selenium.WebDriverException exception) {
            tearDown();
        }
    }

    @Test
    public void footerBusiness() throws InterruptedException {
        String footer_element = "Business";

        try {
            driver.get("https://www.google.com");
            Thread.sleep(1000);

            WebElement link_footer = driver.findElement(By.linkText(footer_element));
            Assert.assertTrue(footer_element, link_footer.getText().contains(footer_element));
            Assert.assertTrue(link_footer.isDisplayed());

        } catch (org.openqa.selenium.WebDriverException exception) {
            tearDown();
        }
    }

    @Test
    public void footerAbout() throws InterruptedException {
        String footer_element = "About";

        try {
            driver.get("https://www.google.com");
            Thread.sleep(1000);

            WebElement link_footer = driver.findElement(By.linkText(footer_element));
            Assert.assertTrue(footer_element, link_footer.getText().contains(footer_element));
            Assert.assertTrue(link_footer.isDisplayed());

        } catch (org.openqa.selenium.WebDriverException exception) {
            tearDown();
        }
    }

    @Test
    public void optionalTaskLogin() throws InterruptedException {
        String button_sign_in = "Sign in";
        WebDriverWait wait = new WebDriverWait(driver, 30);
        String email_id = "identifierId";
        String password_name = "password";
        String button_next_email = "identifierNext";
        String button_next_pass = "passwordNext";
        String username = "jamessellerst1@gmail.com";
        String password = "J@mesSt!1";
        String google_logo = "hplogo";

        try {
            driver.get("https://www.google.com");
            Thread.sleep(1000);

            WebElement google_sign_in = driver.findElement(By.linkText(button_sign_in));
            google_sign_in.click();
            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id(email_id)));
            WebElement email_field = driver.findElement(By.id(email_id));
            email_field.sendKeys(username);
            driver.findElement(By.id(button_next_email)).click();

            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id(button_next_pass)));
            WebElement password_field = driver.findElement(By.name(password_name));
            password_field.sendKeys(password);
            driver.hideKeyboard();
            driver.findElement(By.id(button_next_pass)).click();
            Thread.sleep(5000);
            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id(google_logo)));

        } catch (org.openqa.selenium.WebDriverException exception) {
            tearDown();
        }
    }

    public void tearDown() {
        driver.quit();
    }
}

