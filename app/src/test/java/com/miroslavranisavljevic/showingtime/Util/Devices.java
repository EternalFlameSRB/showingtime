package com.miroslavranisavljevic.showingtime.Util;


/**
 * Created by miroslav on 7/23/18.
 *
 */

public class Devices {
    private String platform;
    private String name;
    private String version;
    private String airplane_on;
    private String airplane_off;

    public Devices(String platform, String name, String version, String airplane_on, String airplane_off){
        this.platform = platform;
        this.name = name;
        this.version = version;
        this.airplane_on = airplane_on;
        this.airplane_off = airplane_off;
    }

    public Devices(String device){
        switch (device) {
            case Constants.NEXUS_5:
                this.platform = "Android";
                this.name = Constants.NEXUS_5;
                this.version = "9";
                this.airplane_on = "Flight,mode,On.,Button";
                this.airplane_off = "Flight,mode,Off.,Button";
                break;
            case Constants.PIXEL:
                this.platform = "Android";
                this.name = Constants.PIXEL;
                this.version = "9";
                this.airplane_on = "Flight,mode,On.,Button";
                this.airplane_off = "Flight,mode,Off.,Button";
                break;
            case Constants.TESLA_SMARTPHONE_6_1:
                this.platform = "Android";
                this.name = Constants.TESLA_SMARTPHONE_6_1;
                this.version = "6.0";
                this.airplane_on = "Aeroplane mode on.";
                this.airplane_off = "Aeroplane mode off.";
                break;
        }
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAirplane_on() {
        return airplane_on;
    }

    public void setAirplane_on(String airplane_on) {
        this.airplane_on = airplane_on;
    }

    public String getAirplane_off() {
        return airplane_off;
    }

    public void setAirplane_off(String airplane_off) {
        this.airplane_off = airplane_off;
    }
}
