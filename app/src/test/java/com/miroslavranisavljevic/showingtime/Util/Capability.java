package com.miroslavranisavljevic.showingtime.Util;

import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;

import io.appium.java_client.remote.MobileCapabilityType;

public class Capability {

    public static DesiredCapabilities capabilitiesWebLaunch(String browser, String device, String version){
        System.out.println("setUp 1 - initialize");

        DesiredCapabilities capabilities = new DesiredCapabilities();

        System.out.println("setUp 2 - capabilities");

        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", device);
        capabilities.setCapability(MobileCapabilityType.VERSION, version);
        System.out.println("setUp 3 - platform and device");

        capabilities.setCapability("chromedriverExecutable", "/home/linuxbrew/.linuxbrew/bin/chromedriver");
        capabilities.setCapability("noReset", true);
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, browser);
        System.out.println("setUp 4 - browser selection");

        return capabilities;
    }

}
