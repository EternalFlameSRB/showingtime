package com.miroslavranisavljevic.showingtime.Util;

/**
 * Created by miroslav on 7/23/18.
 *
 */

public class Constants {

    //Konstante vezane za uredjaje koji se koriste
    public static final String PIXEL = "Pixel";
    public static final String NEXUS_5 = "Nexus5";
    public static final String TESLA_SMARTPHONE_6_1 = "Smartphone6.1";

    //Package
    public static final String PERM_PACKAGE_INSTALLER = "com.android.packageinstaller";
    public static final String PERM_PACKAGE_ACTIVITY = ".permission.ui.GrantPermissionsActivity";
}
